== Compartición de Archivos (Coquelicot) ==

=== Acerca de Coquelicot ===

''Coquelicot'' es aplicación web para compartir archivos enfocada a proteger la privacidad de sus usuarios. El principio básico es simple: los usuarios pueden subir un archivo al servidor y a cambio reciben una URL única para descargarlo que se puede compartir con terceros. Además se puede establecer una contraseña para reforzar el acceso.

Más información acerca de Coquelicot en [[https://coquelicot.potager.org/README|su LEEME]]

'''Disponible desde:'''  versión 0.24.0

=== Cuando usar Coquelicot ===

El mejor uso de Coquelicot es para compartir rápidamente un archivo suelto. 
Si quieres compartir una carpeta... 

 * ...para usar y tirar, comprime la carpeta y compartela como archivo con Coquelicot
 * ...que deba mantenerse sincronizada entre ordenadores usa mejor [[../Syncthing|Syncthing]]


Coquelicot también puede proporcionar un grado de privacidad razonable. Si se necesita anonimato mejor sopesas emplear la aplicación de escritorio [[https://onionshare.org/|Onionshare]].

Como Coquelicot carga todo el archivo al servidor tu !FreedomBox consumirá ancho de banda tanto para la subida como para la descarga. Para archivos muy grandes sopesa compartirlos creando un fichero !BitTorrent privado. Si se necesita anonimato usa Onionshare. Es P2P y no necesita servidor. 

=== Coquelicot en FreedomBox ===

Con Coquelicot instalado puedes subir archivos a tu servidor !FreedomBox y compartirlos en privado. 

Tras la instalación la página de Coquelicot ofrece 2 preferencias.

 1. '''Contraseña de Subida''': Actualmente y por facilidad de uso Coquelicot está configurado en !FreedomBox para usar autenticación simple por contraseña. Recuerda que se trata de una contraseña global para esta instancia de Coquelicot y no tu contraseña de usuario para !FreedomBox. Tienes que acordarte de esta contraseña. Puedes establecer otra en cualquier momento desde el interfaz Plinth. 
 2. '''Tamaño Máximo de Archivo''': Puedes alterar el tamaño máximo de los archivos a transferir mediante Coquelicot usando esta preferencia. El tamaño se expresa en [[https://en.wikipedia.org/wiki/Mebibyte|Mebibytes]] y el máximo solo está limitado por el espacio en disco de tu !FreedomBox. 

=== Privacidad ===
Alguien que monitorice tu tráfico de red podría averiguar que se está transfiriendo un archivo en tu !FreedomBox y posiblemente también su tamaño pero no sabrá su nombre. Coquelicot cifra los archivos en el servidor y sobrescribe los contenidos con 0s al borrarlos, eliminando el riesgo de que se desvelen los contenidos del fichero si tu !FreedomBox resultara confiscada o robada. El riesgo real que hay que mitigar es que además del destinatario legítimo un tercero también descargue tu fichero.

==== Compartir mediante mensajería instantánea ====
Algunas aplicaciones de mensajería instantánea con vista previa de sitios web podrían descargar tu fichero para mostrarla (su vista previa) en la conversación. Si configuras la opción de descarga única para un archivo podrías notar que la aplicación de mensajería consume la única descarga. Si compartes mediante estas aplicaciones usa una contraseña de descarga en combinación con la opción de descarga única. 

==== Compartir en privado enlaces de descarga ====

Se recomienda compartir las contraseñas y los enlaces de descarga de tus archivos por canales cifrados. Puedes evitar todos los problemas anteriores con las vistas previas de la mensajería instantánea símplemente empleando aplicaciones de mensajería que soporten conversaciones cifradas como Riot con [[../MatrixSynapse| Matrix Synapse]] o [[../ejabberd|XMPP]] (servidor ejabberd en !FreedomBox) con clientes que soporten cifrado punto a punto. Envía la contraseña y el enlace de descarga separados en 2 mensajes distintos (ayuda que tu aplicación de mensajería soporte ''perfect forward secrecy'' como XMPP con OTR). También puedes compartir tus enlaces por correo electrónico cifrado con PGP usando [[https://securityinabox.org/en/guide/thunderbird/linux/|Thunderbird]]. 

## END_INCLUDE

Volver a la [[es/FreedomBox/Features|descripción de Funcionalidades]] o a las páginas del [[es/FreedomBox/Manual|manual]].

<<Include(es/FreedomBox/Portal)>>

----
CategoryFreedomBox
